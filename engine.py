import json
import logging
import os
import speech_recognition as sr
import argparse
import whisper
import openai
import assemblyai as aai

parser = argparse.ArgumentParser(
    description="Transcribe audio file using OpenAI's Whisper API or AssemblyAI's API"
)
parser.add_argument(
    "--mode",
    choices=["online", "offline", "assemblyai"],
    default="offline",
    help="Choose transcription mode (online, offline, or assemblyai)",
)
args = parser.parse_args()

config = json.load(open("config.json"))

# Add your OpenAI API key
openai.api_key = config["openai_key"]

# Add your AssemblyAI API key
aai.settings.api_key = config["assemblyai_key"]

transcriber = aai.Transcriber()

folder_path = "audio_cache"
filepath = f"{folder_path}/audio.wav"

logging.basicConfig(filename="transcription.log", level=logging.INFO)


def transcribe_audio_openai(filename: str) -> str:
    """Transcribe audio file using OpenAI's Whisper API

    Parameters
    ----------
    filename : str
        Filename of the audio file to be transcribed

    Returns
    -------
    str
        Transcription of the audio file
    """
    try:
        with open(filename, "rb") as audio_file:
            transcript = openai.Audio.transcribe("whisper-1", audio_file)
        return transcript.text
    except Exception as e:
        logging.error(f"Error transcribing audio file using OpenAI API: {e}")
        raise e

# Define the transcribe_audio_offline function
def transcribe_audio_offline(filename: str) -> str:
    """Transcribe audio file using a local Whisper model

    Parameters
    ----------
    filename : str
        Filename of the audio file to be transcribed

    Returns
    -------
    str
        Transcription of the audio file
    """
    transcript = model.transcribe(filename)
    return transcript["text"]

def transcribe_audio_assemblyai(filename: str) -> str:
    """Transcribe audio file using AssemblyAI's API

    Parameters
    ----------
    filename : str
        Filename of the audio file to be transcribed

    Returns
    -------
    str
        Transcription of the audio file
    """
    try:
        transcript = transcriber.transcribe(filename)
        return transcript.text
    except Exception as e:
        logging.error(f"Error transcribing audio file using AssemblyAI API: {e}")
        raise e


def get_text_from_audio():
    r = sr.Recognizer()
    while True:
        with sr.Microphone() as source:
            logging.info("Say something!")
            r.adjust_for_ambient_noise(source)
            audio = r.listen(source)
            logging.info("Got it! Now to recognize it...")
            try:
                with open(filepath, "wb") as f:
                    f.write(audio.get_wav_data())
            except Exception as e:
                logging.error(f"Error writing audio file: {e}")
                raise e
            try:
                if args.mode == "online":
                    text = transcribe_audio_openai(filepath)
                elif args.mode == "assemblyai":
                    text = transcribe_audio_assemblyai(filepath)
                else:
                    text = transcribe_audio_offline(filepath)
                logging.info(f"You said: {text}")
                print(f"You said: {text}")
            except Exception as e:
                logging.error(f"Error transcribing audio: {e}")
                raise e


if __name__ == "__main__":
    os.makedirs(folder_path, exist_ok=True)
    if args.mode == "offline":
        model = whisper.load_model("small.en")
    get_text_from_audio()
